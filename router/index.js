import express  from 'express';
import  json  from 'body-parser';
import { render } from 'ejs';

export const router = express.Router();

function calcularPago(nivel, pagoHB, horasImp) {
    const incrementos = [0.3, 0.5, 1.0];
    const incremento = incrementos[nivel - 1];
    return pagoHB * (1 + incremento) * horasImp;
}

function calcularImpuesto(pagoPorHoras) {
    return pagoPorHoras * 0.16;
}

function calcularBono(numHijos, pagoPorHoras) {
    let porcBono = 0;
    numHijos = parseInt(numHijos);

    if (numHijos >= 1 && numHijos <= 2) {
        porcBono = 0.05;
    } else if (numHijos >= 3 && numHijos <= 5) {
        porcBono = 0.10;
    } else if (numHijos > 5) {
        porcBono = 0.20;
    }
    return pagoPorHoras * porcBono;
}

function calcularTotalAPagar(pagoPorHoras, bono, impuesto) {
    return pagoPorHoras + bono - impuesto;
}  

// Declarar primer ruta por omisión
// Ruta por omisión
router.get('/', (req, res) => {
    res.render('index', { titulo: "Colegio Patria", nombre: "Jesus Esteban Morales Niebla" });
});

// Ruta para el Pago de Recibo
router.get("/calcularpago", (req, res) => {
    res.render("calcularpago", {
      titulo: "Calculo del Pago",
      isPost: false
    });
}); 

// Ruta para procesar la tabla de Pago de Recibo
router.post('/calcularpago', (req, res) => {
    const { numDocente, nombre, domicilio, nivel, pagoHB, horasImp, numHijos } = req.body;

    const numNivel = parseInt(nivel);
    const pagoHBNum = parseFloat(pagoHB);
    const horasImpNum = parseFloat(horasImp);
    const numHijosNumerico = parseInt(numHijos);

    const pagoPorHoras = calcularPago(parseInt(nivel), parseFloat(pagoHB), parseFloat(horasImp));
    const bono = calcularBono(parseInt(numHijos), pagoPorHoras);
    const impuesto = calcularImpuesto(pagoPorHoras);
    const totalAPagar = calcularTotalAPagar(pagoPorHoras, bono, impuesto);

    const params = {
        titulo: "Calculo del Pago",
        numDocente,
        nombre,
        domicilio,
        nivel: numNivel,
        pagoHB: pagoHBNum,
        horasImp: horasImpNum,
        numHijos: numHijosNumerico,
        pagoPorHoras,
        bono,
        impuesto,
        totalAPagar,
        isPost: true
    };
    res.render("calcularpago", params);
});

export default {router}